//
//  ClassHomeInterfaceController.swift
//  GoodlifeWatchPrototype
//
//  Created by Nathan Power on 23/6/17.
//  Copyright © 2017 Outware Mobile. All rights reserved.
//

import WatchKit
import Foundation


class ClassHomeInterfaceController: WKInterfaceController {

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        StateSingleton.sharedInstance.workoutCompleteDelegate = self
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func showNotifications() {
        presentController(withNames: ["Notification1", "Notification2", "Notification3"], contexts: nil)
    }
    
    @IBAction func performClassSegue() {
        pushController(withName: "ClassWorkout", context: self)
    }

}

extension ClassHomeInterfaceController: WorkoutCompleteDelegate {
    
    func userDidCompleteWorkout(workoutType: WorkoutType) {
        WKInterfaceController.reloadRootControllers(withNames: [workoutType.getWorkoutSummaryInterfaceControllerString()], contexts: nil)
        popToRootController()
    }
    
    func workoutSaved(wasShared: Bool) {
        if wasShared {
            // show share success UI?
        }
        animate(withDuration: 0.2, animations: { [weak self] in
            self?.popToRootController()
        })
    }
}
