//
//  WorkoutCompleteDelegate.swift
//  GoodlifeWatchPrototype
//
//  Created by Nathan Power on 23/6/17.
//  Copyright © 2017 Outware Mobile. All rights reserved.
//

import Foundation

protocol WorkoutCompleteDelegate {
    func userDidCompleteWorkout(workoutType: WorkoutType)
    func workoutSaved(wasShared: Bool)
}
