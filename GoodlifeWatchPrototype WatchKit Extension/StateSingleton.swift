//
//  StateSingleton.swift
//  GoodlifeWatchPrototype
//
//  Created by Nathan (Outware) on 22/6/17.
//  Copyright © 2017 Outware Mobile. All rights reserved.
//

import Foundation


final class StateSingleton {

  var userIsLoggedIn = false

  var userCheckinDelegate: UserCheckinDelegate?
  var workoutCompleteDelegate: WorkoutCompleteDelegate?
  var customWorkoutOverviewController: CustomWorkOverviewInterfaceController?
  var customWoroutPerformed: Bool?

  static let sharedInstance = StateSingleton()
  private init() {}

}
