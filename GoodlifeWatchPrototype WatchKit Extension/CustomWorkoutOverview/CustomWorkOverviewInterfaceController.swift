//
//  CustomWorkOverviewInterfaceController.swift
//  GoodlifeWatchPrototype
//
//  Created by Nathan (Outware) on 22/6/17.
//  Copyright © 2017 Outware Mobile. All rights reserved.
//

import WatchKit
import Foundation


class CustomWorkOverviewInterfaceController: WKInterfaceController {

  @IBOutlet var checkBoxesImageButton: WKInterfaceButton!
  @IBOutlet var overviewImage: WKInterfaceImage!

  override func awake(withContext context: Any?) {
    super.awake(withContext: context)

    // Configure interface objects here.
  }

  override func willActivate() {
    // This method is called when watch view controller is about to be visible to user
    super.willActivate()
    StateSingleton.sharedInstance.customWorkoutOverviewController = self
  }

  override func didDeactivate() {
    // This method is called when watch view controller is no longer visible
    super.didDeactivate()
  }

  @IBAction func endWorkoutButtonTapped() {
    StateSingleton.sharedInstance.customWoroutPerformed = true
    StateSingleton.sharedInstance.workoutCompleteDelegate?.userDidCompleteWorkout(workoutType: .custom)
  }

  func benchpressDidComplete() {
    dismiss()
    pop()
    overviewImage.setImageNamed("glw_custom_workout_checked")
    checkBoxesImageButton.setEnabled(false)
  }
}
