//
//  CustomWorkoutInProgressInterfaceController.swift
//  GoodlifeWatchPrototype
//
//  Created by Nathan (Outware) on 22/6/17.
//  Copyright © 2017 Outware Mobile. All rights reserved.
//

import WatchKit
import Foundation


class CustomWorkoutInProgressInterfaceController: WKInterfaceController {
    
    enum WorkoutProgressState: Int {
        case reps4 = 0, rest45, ready, reps10, done
        
        func getImageForState() -> String {
            switch self {
            case .reps4:
                return "glw_reps4"
            case .rest45:
                return "glw_rest"
            case .ready:
                return "glw_customWorkoutReady"
            case .reps10:
                return "glw_reps10"
            case .done:
                return "glw_customWorkoutDone"
                
            }
        }
        
        func shouldAllowTapTransition() -> Bool {
            switch self {
            case .reps4:
                return false
            case .rest45:
                return true
            case .ready:
                return true
            case .reps10:
                return false
            case .done:
                return true
                
            }
        }
    }
    
    @IBOutlet var workoutProgressButton: WKInterfaceButton!
    
    var workoutProgress: WorkoutProgressState = .reps4
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        updateWorkoutStatusImage()
    }

    @IBAction func finshCustomWorkoutTapped() {
        StateSingleton.sharedInstance.customWorkoutOverviewController?.benchpressDidComplete()
    }
    
    @IBAction func workoutProgressNextForceTouch() {
        moveToNextWorkoutState()
    }

    @IBAction func workoutProgressButtonTapped() {
        if workoutProgress.shouldAllowTapTransition() {
            moveToNextWorkoutState()
        }
    }
    
    func moveToNextWorkoutState() {
        if let nextWorkoutState = WorkoutProgressState(rawValue: workoutProgress.hashValue + 1) {
            workoutProgress = nextWorkoutState
            updateWorkoutStatusImage()
        } else {
            // end of workout
            StateSingleton.sharedInstance.customWorkoutOverviewController?.benchpressDidComplete()
        }
    }
    
    func updateWorkoutStatusImage() {
        workoutProgressButton.setBackgroundImageNamed(workoutProgress.getImageForState())
    }
}
