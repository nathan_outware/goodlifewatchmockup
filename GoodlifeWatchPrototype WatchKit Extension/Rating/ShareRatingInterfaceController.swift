//
//  RatingInterfaceController.swift
//  GoodlifeWatchPrototype
//
//  Created by Roy Schmid on 26/06/2017.
//  Copyright © 2017 Outware Mobile. All rights reserved.
//

import WatchKit
import Foundation


class ShareRatingInterfaceController: WKInterfaceController {

  @IBOutlet var shareRating: WKInterfaceButton!

  override func awake(withContext context: Any?) {
    super.awake(withContext: context)
    // Configure interface objects here.
  }

  override func willActivate() {
    // This method is called when watch view controller is about to be visible to user
    super.willActivate()
  }

  override func didDeactivate() {
    // This method is called when watch view controller is no longer visible
    super.didDeactivate()
  }
  
  @IBAction func shareRatingTapped() {
    WKInterfaceController.reloadRootControllers(withNames: ["ClassHomeInterface"], contexts: nil)
    popToRootController()
  }
}
