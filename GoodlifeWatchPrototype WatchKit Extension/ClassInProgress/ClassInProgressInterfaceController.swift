//
//  ClassInProgressInterfaceController.swift
//  GoodlifeWatchPrototype
//
//  Created by Nathan (Outware) on 22/6/17.
//  Copyright © 2017 Outware Mobile. All rights reserved.
//

import WatchKit
import Foundation


class ClassInProgressInterfaceController: WKInterfaceController {

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func finishTapped() {
        StateSingleton.sharedInstance.customWoroutPerformed = false
        StateSingleton.sharedInstance.workoutCompleteDelegate?.userDidCompleteWorkout(workoutType: .crossfit)
    }
    
}
