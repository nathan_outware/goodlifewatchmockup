//
//  SharingTableRow.swift
//  GoodlifeWatchPrototype
//
//  Created by Nathan (Outware) on 22/6/17.
//  Copyright © 2017 Outware Mobile. All rights reserved.
//

import WatchKit

class SharingTableRow: NSObject {
    @IBOutlet var sharingOptionSwitch: WKInterfaceSwitch!
}
