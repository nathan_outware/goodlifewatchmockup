//
//  SharingInterfaceController.swift
//  GoodlifeWatchPrototype
//
//  Created by Nathan (Outware) on 22/6/17.
//  Copyright © 2017 Outware Mobile. All rights reserved.
//

import WatchKit
import Foundation

struct SharingRowData {
    var name: String
    var active: Bool
}

class SharingInterfaceController: WKInterfaceController {

    @IBOutlet var sharingButton: WKInterfaceButton!
    @IBOutlet var sharingOptionsTable: WKInterfaceTable!
    
    let sharingData: [SharingRowData] = [SharingRowData(name: "Goodlife", active: true),
                                         SharingRowData(name: "Facebook", active: false)]
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        configureTableData(data: sharingData)
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func configureTableData(data: [SharingRowData]) {
        sharingOptionsTable.setNumberOfRows(data.count, withRowType: "SharingOption")
        
        var i = 0
        while i < sharingOptionsTable.numberOfRows {
            if let rowController = sharingOptionsTable.rowController(at: i) as? SharingTableRow {
                rowController.sharingOptionSwitch.setTitle(data[i].name)
                rowController.sharingOptionSwitch.setOn(data[i].active)
            }
            i += 1
        }
    }

    @IBAction func shareTapped() {
      guard let customWorkout = StateSingleton.sharedInstance.customWoroutPerformed else {
        fatalError()
      }

      if customWorkout {
        WKInterfaceController.reloadRootControllers(withNames: ["ClassHomeInterface"], contexts: nil)
      } else {
        WKInterfaceController.reloadRootControllers(withNames: ["RatingInterfaceController"], contexts: nil)
      }
    }
}
