//
//  SummaryInterfaceController.swift
//  GoodlifeWatchPrototype
//
//  Created by Nathan Power on 23/6/17.
//  Copyright © 2017 Outware Mobile. All rights reserved.
//

import WatchKit
import Foundation


class SummaryInterfaceController: WKInterfaceController {

  @IBAction func saveDismissSummary() {

    guard let customWorkoutPerformed = StateSingleton.sharedInstance.customWoroutPerformed else {
      fatalError("customWorkoutPerformed property was not set!")
    }

    if customWorkoutPerformed {
      goToHomeScreen()
    } else {
      WKInterfaceController.reloadRootControllers(withNames: ["RatingInterfaceController"], contexts: nil)
      popToRootController()
    }
  }

  func goToHomeScreen () {
    if StateSingleton.sharedInstance.userIsLoggedIn {
      WKInterfaceController.reloadRootControllers(withNames: ["ClassHomeInterface"], contexts: nil)
      popToRootController()
    } else {
      WKInterfaceController.reloadRootControllers(withNames: ["WelcomeInterface"], contexts: nil)
      popToRootController()
    }
  }

}
