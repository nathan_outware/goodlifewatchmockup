//
//  ComplicationsInterfaceController.swift
//  GoodlifeWatchPrototype
//
//  Created by Nathan Power on 23/6/17.
//  Copyright © 2017 Outware Mobile. All rights reserved.
//

import WatchKit
import Foundation


class ComplicationsInterfaceController: WKInterfaceController {

    @IBAction func goToHomeScreen() {
        animate(withDuration: 0.5, animations: {
            WKInterfaceController.reloadRootControllers(withNames: ["WelcomeInterface"], contexts: nil)
        })
        
    }

}
