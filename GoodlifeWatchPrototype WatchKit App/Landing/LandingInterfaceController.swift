//
//  LandingInterfaceController.swift
//  GoodlifeWatchPrototype
//
//  Created by Nathan (Outware) on 22/6/17.
//  Copyright © 2017 Outware Mobile. All rights reserved.
//

import WatchKit
import Foundation

protocol UserCheckinDelegate: class {
    func userDidCheckIn()
    
}


enum WorkoutType: String {
    case crossfit, custom
    
    func getWorkoutSummaryInterfaceControllerString() -> String {
        switch self {
        case .crossfit:
            return "CrossfitWorkoutSummary"
        default:
            return "CustomWorkoutSummary"
        }
    }
}

class LandingInterfaceController: WKInterfaceController {
    
    @IBOutlet var checkInButton: WKInterfaceButton!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        StateSingleton.sharedInstance.userCheckinDelegate = self
        StateSingleton.sharedInstance.workoutCompleteDelegate = self
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    override func contextForSegue(withIdentifier segueIdentifier: String) -> Any? {
        switch segueIdentifier {
        case "CheckInSegue":
            return self
        default:
            return nil // nothing
            
        }
    }
    
    @IBAction func checkInButtonTapped() {
        if !StateSingleton.sharedInstance.userIsLoggedIn {
            presentController(withName: "CheckInInterface", context: self)
        } else {
            pushController(withName: "ClassSelectorWorkout", context: self)
        }
    }
    
    @IBAction func showNotifications() {
        presentController(withNames: ["Notification1", "Notification2", "Notification3"], contexts: nil)
    }
    
}

extension LandingInterfaceController: UserCheckinDelegate {
    
    func userDidCheckIn() {
        StateSingleton.sharedInstance.userIsLoggedIn = true
        WKInterfaceController.reloadRootControllers(withNames: ["ClassHomeInterface"], contexts: nil)
        dismiss()
    }
    
}

extension LandingInterfaceController: WorkoutCompleteDelegate {
    
    func userDidCompleteWorkout(workoutType: WorkoutType) {
        WKInterfaceController.reloadRootControllers(withNames: [workoutType.getWorkoutSummaryInterfaceControllerString()], contexts: nil)
        popToRootController()
    }
    
    func workoutSaved(wasShared: Bool) {
        if wasShared {
            // show share success UI?
        }
        
        WKInterfaceController.reloadRootControllers(withNames: ["ClassHomeInterface"], contexts: nil)
        popToRootController()

    }
}


