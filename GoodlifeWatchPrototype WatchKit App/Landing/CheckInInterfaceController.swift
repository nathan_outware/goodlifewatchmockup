//
//  CheckInInterfaceController.swift
//  GoodlifeWatchPrototype
//
//  Created by Nathan (Outware) on 22/6/17.
//  Copyright © 2017 Outware Mobile. All rights reserved.
//

import WatchKit
import Foundation


class CheckInInterfaceController: WKInterfaceController {

    @IBOutlet var qrButton: WKInterfaceButton!
    
    var buttonTapCount: Int = 0
    weak var delegate: UserCheckinDelegate?
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        self.delegate = context as? UserCheckinDelegate
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func buttonWasTapped() {
        switch buttonTapCount {
        case 0:
            qrButton.setBackgroundImageNamed("glw_checkInConfirmation")
            buttonTapCount += 1
        default:
            delegate?.userDidCheckIn()
        }
    }
}

